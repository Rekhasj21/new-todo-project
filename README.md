
<h1 align="center">Todo Application</h1>

## Table of Contents

- [Overview](#overview)
- [Project Link](#project-link)
- [Built With](#built-with)
- [How to use](#how-to-use)
- [Contact](#contact)

## Overview

<a href="" rel="image text"><img src="src/todoProject.png" alt="" /></a>

- User story: We can add a new task
- User story: We can complete a task
- User story: We can toggle between All, Active and Completed
- User story: We can remove one or all tasks under the Completed tab

## Project Link

<a href="https://rekha-todo-project.netlify.app/" >Click To Check Project Link</a>

### Built With

- HTML
- CSS
- JavaScript

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone git@gitlab.com:Rekhasj21/new-todo-project.git

# Install dependencies
$ npm install

# Run the app
$ npm start
```

## Contact

- Website (<https://rekha-todo-project.netlify.app/>)
- GitHub (<https://gitlab.com/Rekhasj21/new-todo-project>)
