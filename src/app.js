import { displayTodoList } from "./displayTodoList.js";
const addTodo = document.getElementById('addTodo')

let todoListCount = 0;
let todoList = [];

addTodo.onclick = function (event) {
    event.preventDefault()

    let userEnteredTask = document.getElementById('userEnteredTask');
    let userInputTask = userEnteredTask.value;
    todoListCount = todoListCount + 1;
    let todo = {
        text: userInputTask,
        uniqueNo: todoListCount,
        isActive: true,
    }
    todoList.push(todo);
    displayTodoList(todoList);
    userEnteredTask.value = '';
}

function deleteTodoItem(id) {
    let index = todoList.findIndex((list) => list.uniqueNo === id);
    todoList.splice(index, 1);
    const completedTodoList = todoList.filter(todo => !todo.isActive);
    displayTodoList(completedTodoList)
}

function toggleTodoStatus(todo) {
    todo.isActive = !todo.isActive;
    displayTodoList(todoList)
}

function filterTodoList(status) {
    if (status === "active") {
        return todoList.filter((todo) => todo.isActive)
    } else if (status === "completed") {
        return todoList.filter((todo) => !todo.isActive);
    } else {
        return todoList;
    }
}

function deleteAllCompletedTodoItems() {
    todoList = todoList.filter((todo) => todo.isActive);
    return todoList
}

export { deleteTodoItem, toggleTodoStatus, filterTodoList, deleteAllCompletedTodoItems };