import { filterTodoList, deleteAllCompletedTodoItems, deleteTodoItem } from './app'

const todoListContainer = document.getElementById("todoListContainer")
let isCompletedClicked

function createTodoItem(todo) {
  let id = todo.uniqueNo;
  let todoId = 'todo' + id;
  let labelId = 'label' + id;
  let checkboxId = 'checkbox' + id;

  let todoListItem = document.createElement('li');
  todoListItem.classList.add('todo-items');
  todoListItem.id = todoId;
  todoListContainer.appendChild(todoListItem);

  let inputElement = document.createElement('input');
  inputElement.type = 'checkbox';
  inputElement.id = checkboxId;
  inputElement.classList.add("checkbox");
  inputElement.checked = !todo.isActive;
  todoListItem.appendChild(inputElement);

  let labelElement = document.createElement('label');
  labelElement.setAttribute('for', checkboxId);
  labelElement.classList.add('label');
  labelElement.id = labelId;
  labelElement.textContent = todo.text;
  todoListItem.appendChild(labelElement);

  if (!todo.isActive) {
    labelElement.classList.add('checkbox-strike');
  }

  if (!todo.isActive && isCompletedClicked) {
    labelElement.classList.add('checkbox-strike');
    let deleteButtonElement = document.createElement("span");
    deleteButtonElement.textContent = "delete_outline";
    deleteButtonElement.classList.add("material-icons", "material");
    todoListItem.appendChild(deleteButtonElement);

    deleteButtonElement.onclick = function () {
      deleteTodoItem(id)
    };
  }

  inputElement.onclick = function () {
    labelElement.classList.toggle('checkbox-strike');
    todo.isActive = !inputElement.checked;
  }
}

function toggleActive(button) {
  allSelected.classList.remove('active-list');
  activeSelected.classList.remove('active-list');
  completedSelected.classList.remove('active-list');
  button.classList.add('active-list');
}

allSelected.addEventListener('click', function () {
  document.getElementById('deleteAllIconContainer').style.visibility = 'hidden'
  document.getElementById("inputContainer").style.display = 'block'
  isCompletedClicked = false
  toggleActive(allSelected);
  const allTodoList = filterTodoList();
  displayTodoList(allTodoList)
});

activeSelected.addEventListener('click', function () {
  document.getElementById('deleteAllIconContainer').style.visibility = 'hidden'
  document.getElementById("inputContainer").style.display = 'block'
  const activeTodoList = filterTodoList("active")
  toggleActive(activeSelected);
  displayTodoList(activeTodoList);

});

completedSelected.addEventListener('click', function () {
  document.getElementById('deleteAllIconContainer').style.visibility = 'visible'
  document.getElementById("inputContainer").style.display = 'none'
  const completedTodoList = filterTodoList("completed")
  isCompletedClicked = true
  toggleActive(completedSelected);
  displayTodoList(completedTodoList);
});

document.getElementById("deleteAllIconContainer").onclick = function () {
  const list = deleteAllCompletedTodoItems()
  displayTodoList();
}

export function displayTodoList(todoList) {
  todoListContainer.textContent = '';
  for (let todo of todoList) {
    createTodoItem(todo);
  }
}
